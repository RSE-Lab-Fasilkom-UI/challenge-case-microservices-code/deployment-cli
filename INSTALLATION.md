# Installation Guides ABS Microservices Deployment CLI

Reliable Software Engineering, Faculty of Computer Science, University of Indonesia (2020)

It is preferable to perform these following steps using `root` user.


1.  Install prerequisites
    ```bash
    sudo apt update
    sudo apt upgrade
    sudo apt install nginx python3 python3-pip default-jre default-jdk git postgresql
    ```

2.  Download Apache Ant
    ```bash
    wget https://downloads.apache.org//ant/binaries/apache-ant-1.10.8-bin.tar.gz
    mv apache-ant-1.10.8-bin.tar.gz /usr/share/.
    mv apache-ant-1.10.8-bin.tar.gz apache-ant.tar.gz
    tar -zxvf apache-ant.tar.gz
    mv apache-ant-1.10.8 apache-ant
    rm apache-ant.tar.gz
    ```

3.  Add Apache Ant bin to PATH
    ```bash
    cd /usr/bin
    ln -s /usr/share/apache-ant/bin/ant /usr/bin/ant
    ```

    ```bash
    export ANT_HOME=/usr/local/apache-ant
    export PATH=${ANT_HOME}/bin:${PATH}
    ```

4.  Restart the VM and later check whether Ant already being added to PATH

5.  Download All ABS Microservices Projects and store it inside a folder
    ```bash
    mkdir abs-microservices-projects
    cd abs-microservices-projects/
    git clone https://gitlab.com/RSE-Lab-Fasilkom-UI/challenge-case-microservices-code/cart-abs-microservices.git
    git clone https://gitlab.com/RSE-Lab-Fasilkom-UI/challenge-case-microservices-code/catalog-abs-microservices.git
    mv cart-abs-microservices Cart
    mv catalog-abs-microservices Catalog
    ```

6.  Download Deployment CLI Tool and Create a Configuration Folder
    ```bash
    git clone https://gitlab.com/RSE-Lab-Fasilkom-UI/challenge-case-microservices-code/deployment-cli.git
    cd deployment-cli
    mkdir product_config
    ```

7.  Install the dependencies of Deployment CLI Tool
    ```bash
    cd deployment-cli
    python3 -m pip install virtualenv
    python3 -m pip virtualenv venv
    source venv/bin/activate
    pip install -r requirements.txt
    ```

8.  Create a configuration file specifying prerequisite to automate build and deployment process
    ```bash
    cp config.sample.ini config.ini
    nano config.ini
    ```

    Content of the `config.ini`

    ```ini
    ...
    [work_paths]
    deploy_location = ../generated-microservices/products
    nginx_location = /etc/nginx/sites-enabled
    log_backup_location = _log_backup
    product_config_location = product_config

    [source_paths]
    backend = ../abs-microservices-projects

    [database]
    type = postgresql
    host = localhost
    port = 5432
    root_username = postgres
    root_password = postgres
    root_database = postgres
    ...

    ```