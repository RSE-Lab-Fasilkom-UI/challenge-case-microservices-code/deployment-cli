from deployment import initiate_instance_objects
from utils.administration import check_product_exists, read_json_payload
from utils.ports import reserve_ports


def run_command(args, config):
    if not check_product_exists(args, config):
        raise FileNotFoundError(
            "Project %s not found. Please check deployment path in your config.ini!"
            % (args.product_name, ))

    payload = read_json_payload(args.product_name, config)
    ports = payload["ports"]
    instances = initiate_instance_objects(payload, config)
    instances, ports = __filter_not_running_instances(instances, ports)
    sockets = reserve_ports(ports, exclude=["nginx"])
    start_non_nginx_instances(instances, sockets)


def __filter_not_running_instances(instances, ports):
    filtered_instances = {}
    filtered_ports = {}
    for key in instances:
        if not instances[key].is_running():
            filtered_instances[key] = instances[key]
            filtered_ports[key] = ports[key]
    return filtered_instances, filtered_ports


def start_non_nginx_instances(instances, sockets):
    for key in instances:
        if key != "nginx":
            instances[key].run(sockets[key])
