import os
from datetime import datetime, timedelta
from deployment import initiate_instance_objects
from deployment.stop import stop_instances
from utils.administration import read_json_payload


def run_command(args, config):
    inactive_products = __get_inactive_products_list(args, config)
    __stop_inactive_products(inactive_products, config)


def __parse_relative_time(relative_time_str):
    digit = int(relative_time_str[:-1])
    suffix = relative_time_str[-1]

    if suffix == "w":
        return timedelta(weeks=digit)
    if suffix == "d":
        return timedelta(days=digit)
    if suffix == "h":
        return timedelta(hours=digit)
    if suffix == "m":
        return timedelta(minutes=digit)
    if suffix == "s":
        return timedelta(seconds=digit)


def __get_inactive_products_list(args, config):
    PRODUCTS_PATH = os.path.abspath(
        os.path.expanduser(config["work_paths::deploy_location"]))
    NOW = datetime.now()
    inactive_products = dict()

    for product_dir in os.listdir(PRODUCTS_PATH):
        try:
            full_dir = os.path.join(PRODUCTS_PATH, product_dir)
            log_path = os.path.join(full_dir, config["logs::nginx_access"])
            last_access = __get_last_access_from_log(log_path)
            interval = __parse_relative_time(args.interval)
            if last_access and NOW - last_access > interval:
                inactive_products[full_dir] = last_access
        except FileNotFoundError:
            pass
    return inactive_products


def __get_last_access_from_log(log_path):
    with open(log_path, "r") as f:
        lines = f.read().strip().split("\n")
        for i in range(len(lines) - 1, -1, -1):
            if " 200 " in lines[i]:
                raw_str = lines[i].split(" ")[3].strip()
                return datetime.strptime(raw_str, '[%d/%b/%Y:%H:%M:%S')


def __stop_inactive_products(inactive_products, config):
    print("List of stopped inactive products:")
    for product in inactive_products:
        payload = read_json_payload(os.path.basename(product), config)
        print('"%s", last active: %s' %
              (payload["product_name"], str(inactive_products[product])))
        instances = initiate_instance_objects(payload, config)
        stop_instances(instances)
