import socket
import time


class Port:
    def __init__(self, name, port=0):
        self.name = name
        self.port = port
        self.__socket = None
        self.__reserved = False

    def assign(self):
        if self.port != 0:
            print("WARNING: Port already assigned.")
            return self.port
        return self.reserve(0)

    def reserve(self, port=None):
        if port is None:
            port = self.port
        while not self.__reserved:
            try:
                self.__socket = socket.socket(socket.AF_INET,
                                              socket.SOCK_STREAM)
                self.__socket.bind(("", port))
                self.__reserved = True
            except OSError:
                print("Port for %s: %d already in use. Retrying..." %
                      (self.name, port))
                time.sleep(1)
        if port == 0:
            self.__extract_port()
            port = self.port
        return port

    def is_reserved(self):
        return self.__reserved

    def release(self):
        if self.__socket:
            self.__socket.close()
            self.__socket = None
        self.__reserved = False

    def __extract_port(self):
        if self.__socket:
            self.port = self.__socket.getsockname()[1]

    def __str__(self):
        return str(self.port)


def assign_ports(process_names=None):
    ports = dict()
    for name in process_names:
        ports[name] = Port(name)
        port_number = ports[name].assign()
        print("%s will be assigned to port %d" % (name, port_number))
    return ports


def reserve_ports(ports=None, exclude=None):
    port_objs = dict()
    for name in ports:
        if name not in exclude:
            port = Port(name, port=ports[name])
            port.reserve()
            if port.is_reserved:
                port_objs[name] = port
    return port_objs
