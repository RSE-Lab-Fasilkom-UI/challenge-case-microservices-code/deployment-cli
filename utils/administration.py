import json
import os
import hashlib
import random
import re
import time
from functools import reduce
from utils.db import query_to_database
from utils.template import fill_template
from utils.product_def import (read_basic_feature_config,
                               read_advanced_feature_config)

APP_ROOT_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


def check_product_exists(args, config):
    DEPLOY_ROOT = os.path.abspath(
        os.path.expanduser(config["work_paths::deploy_location"]))
    PRODUCT_ROOT = os.path.join(DEPLOY_ROOT, normalize_name(args.product_name))
    return os.path.exists(PRODUCT_ROOT)


def build_payload(args, config):
    payload = dict()

    payload["product_name"] = normalize_name(args.product_name)
    payload["original_product_name"] = args.product_name

    payload["basic_features"] = read_basic_feature_config(
        config, args.product_name)
    payload["advanced_features"] = {}
    for root_feature in payload["basic_features"]:
        payload["advanced_features"][
            root_feature] = read_advanced_feature_config(
                config, args.product_name, root_feature)

    payload["roles"] = __get_product_roles(args)
    payload["auth"] = __get_oauth2_params(args)

    payload["db_username"] = config["database::root_username"]
    payload["db_password"] = config["database::root_password"]
    return payload


def read_json_payload(product_name, config):
    DEPLOY_ROOT = os.path.abspath(
        os.path.expanduser(config["work_paths::deploy_location"]))
    destination = os.path.join(DEPLOY_ROOT, normalize_name(product_name))
    json_path = os.path.join(destination,
                             normalize_name(product_name) + ".json")

    with open(json_path, "r") as f:
        return json.loads(f.read())


def write_json_payload(payload, config):
    DEPLOY_ROOT = os.path.abspath(
        os.path.expanduser(config["work_paths::deploy_location"]))
    PRODUCT_ROOT = os.path.join(DEPLOY_ROOT, payload["product_name"])
    if not os.path.exists(PRODUCT_ROOT):
        os.makedirs(PRODUCT_ROOT, exist_ok=True)

    json_path = os.path.join(PRODUCT_ROOT, payload["product_name"] + ".json")
    with open(json_path, "w" if os.path.exists(json_path) else "a") as f:
        f.write(json.dumps(payload, indent=4))


def normalize_name(product_name):
    word = []
    for x in product_name.split(" "):
        word.append(x[0].upper() + x[1:])
    return re.sub(r'[^\w]', '', " ".join(word))


def randomize_password(length=12):
    try:
        randomizer = random.SystemRandom()
    except NotImplementedError:
        randomizer = random
        print('A secure pseudo-random number generator is not available '
              'on your system. Falling back to Mersenne Twister.')
        state = randomizer.getstate()
        seed = (int(str(reduce((lambda x, y: x * y), state[1])).rstrip('0')) +
                sum(state[1]))**state[0]
        randomizer.seed(
            hashlib.sha256(('%s%s' % (seed, time.time())).encode()).digest())

    chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
    return ''.join(randomizer.choice(chars) for i in range(length))


def register_product(payload, user_data, config):
    variables = dict(payload)
    variables["user"] = user_data
    variables["database"] = config.get("database", "splelive_administration")
    query = fill_template(os.path.join(APP_ROOT_PATH, "templates",
                                       "register_product.template.sql"),
                          variables,
                          write_to_file=False)
    return query_to_database(query, config)


def unregister_product(payload, config):
    variables = dict(payload)
    variables["database"] = config.get("database", "splelive_administration")
    query = fill_template(os.path.join(APP_ROOT_PATH, "templates",
                                       "unregister_product.template.sql"),
                          variables,
                          write_to_file=False)
    return query_to_database(query, config)


def __extract_product_features(line):
    words = line.split(" ")
    return [
        x.strip() for x in " ".join(words[2:]).replace("(", "").replace(
            ");", "").split(",")
    ]


def __get_product_roles(args):
    if args.roles_file_path:
        with open(args.roles_file_path, "r") as f:
            return json.loads(f.read().strip())
    return json.loads(args.roles)


def __get_product_subdomain(args):
    if args.product_subdomain:
        return args.product_subdomain
    return normalize_name(args.product_name.lower())


def __get_oauth2_params(args):
    AVAILABLE_TYPES = ["auth0", "google", "facebook"]
    oauth2_payload = {}
    oauth2_payload["type"] = args.oauth2_type.lower()
    if args.oauth2_type in AVAILABLE_TYPES:
        oauth2_payload["client_id"] = args.oauth2_client_id
        oauth2_payload["client_secret"] = args.oauth2_client_secret
    return oauth2_payload
