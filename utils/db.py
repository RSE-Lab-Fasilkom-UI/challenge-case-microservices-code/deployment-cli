def connect_to_database(config, database_name=None):
    db_type = config.get("database::type", "mysql").lower()
    if db_type == "mysql":
        return __connect_to_mysql(config)
    if db_type == "postgresql":
        return __connect_to_postgresql(config, database_name)
    return None


def query_to_database(query: str, config, database_name=None):
    db_type = config.get("database::type", "mysql").lower()
    result = None
    if db_type == "mysql":
        conn = __connect_to_mysql(config)
        result = __query_to_mysql(query, conn)
        conn.close()
    elif db_type == "postgresql":
        conn = __connect_to_postgresql(config, database_name)
        result = __query_to_postgresql(query, conn)
        conn.close()
    return result


def __connect_to_mysql(config):
    import mysql.connector
    return mysql.connector.connect(
        host=config.get("database::host", "localhost"),
        port=int(config.get("database::port", "3306")),
        user=config.get("database::root_username", "root"),
        passwd=config.get("database::root_password", ""))


def __query_to_mysql(query: str, conn):
    op_result = conn.cursor(buffered=True).execute(query, multi=True)
    result = []
    try:
        for cur in op_result:
            if cur.with_rows:
                result.append(cur.fetchall())
            else:
                result.append(None)
    except RuntimeError:
        pass
    conn.commit()
    return result


def __connect_to_postgresql(config, database_name=None):
    import psycopg2
    from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
    if not database_name:
        database_name = config.get("database::root_database",
                                   "splelive_administration")
    connection = psycopg2.connect(
        host=config.get("database::host", "localhost"),
        port=int(config.get("database::port", "5432")),
        user=config.get("database::root_username", "postgres"),
        password=config.get("database::root_password", "postgres"),
        database=database_name)
    connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    return connection


def __query_to_postgresql(query: str, conn):
    import psycopg2
    cur = conn.cursor()
    result = []
    for line in query.split(";"):
        _query = line.strip()
        if _query:
            cur.execute(line.strip())
            try:
                result.append(cur.fetchall())
            except psycopg2.ProgrammingError:
                result.append(None)
    return result
