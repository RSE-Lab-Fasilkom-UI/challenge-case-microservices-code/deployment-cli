import os
import shutil
import signal
import sys
import time
from subprocess import Popen, call

PLATFORM = sys.platform


def get_script_extension():
    if PLATFORM == "win32":
        return ".bat"
    return ".sh"


def get_python_exec():
    if PLATFORM == "win32":
        return "python"
    return "python3"


def get_log_redirection(path, stderr_only=False):
    log_path = os.path.abspath(path)
    if not stderr_only:
        return ['>>"%s"' % (log_path, ), "2>&1"]
    if PLATFORM == "win32":
        return [">NUL", '2>>"%s"' % (log_path, )]
    return [">/dev/null", '2>>"%s"' % (log_path, )]


def read_pid_file(dir_path):
    path = os.path.join(dir_path, "instance.pid")
    try:
        with open(path, "r") as f:
            return int(f.read().strip())
    except (IOError, ValueError):
        return None


def write_pid_to_file(proc, dir_path):
    path = os.path.join(dir_path, "instance.pid")
    if proc:
        pid = proc.pid
        with open(path, "w" if os.path.exists(path) else "a") as f:
            f.write(str(pid))
        return pid
    try:
        os.remove(path)
    except IOError:
        pass


def run_process(command_line, wait=False, shell=False, log=None, cwd=None):
    if PLATFORM == "win32":
        from subprocess import CREATE_NEW_PROCESS_GROUP
        cflag = CREATE_NEW_PROCESS_GROUP
        if wait:
            return call(command_line,
                        shell=shell,
                        creationflags=cflag,
                        stdout=log,
                        stderr=log,
                        cwd=cwd)
        return Popen(command_line,
                     shell=shell,
                     creationflags=cflag,
                     stdout=log,
                     stderr=log,
                     cwd=cwd)
    pfn = os.setsid
    if wait:
        return call(command_line,
                    shell=shell,
                    preexec_fn=pfn,
                    stdout=log,
                    stderr=log,
                    cwd=cwd)
    return Popen(command_line,
                 shell=shell,
                 preexec_fn=pfn,
                 stdout=log,
                 stderr=log,
                 cwd=cwd)


def stop_process(pid, sig=signal.SIGTERM):
    try:
        if PLATFORM != "win32":
            pgid = os.getpgid(pid)
            os.killpg(pgid, sig)
        else:
            os.kill(pid, sig)
    except (OSError, ProcessLookupError):
        print("Instance process not found or missing! Skipping...")


def backup_logs(payload, config):
    DEPLOY_ROOT = os.path.abspath(
        os.path.expanduser(config["work_paths::deploy_location"]))
    PRODUCT_ROOT = os.path.join(DEPLOY_ROOT, payload["product_name"])
    LOG_BACKUP_ROOT = os.path.abspath(
        os.path.expanduser(config["work_paths::log_backup_location"]))
    destination = os.path.join(
        LOG_BACKUP_ROOT, "%s_%d" % (payload["product_name"], int(time.time())))
    if not os.path.exists(destination):
        os.makedirs(destination)

    for _ in config["logs"]:
        for root, _, files in os.walk(PRODUCT_ROOT):
            for _file in files:
                if _file.endswith(".log"):
                    log_path = os.path.join(root, _file)
                    base_name = os.path.basename(log_path)
                    shutil.copy(log_path, os.path.join(destination, base_name))
