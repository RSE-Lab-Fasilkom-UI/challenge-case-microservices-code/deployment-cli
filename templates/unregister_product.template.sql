DELETE FROM {% if database.type.lower() != "postgresql" %}{{database.root_database}}.{% endif %}products_in_progress WHERE name='{{product_name}}';
DELETE FROM {% if database.type.lower() != "postgresql" %}{{database.root_database}}.{% endif %}generated_products WHERE name='{{product_name}}';
