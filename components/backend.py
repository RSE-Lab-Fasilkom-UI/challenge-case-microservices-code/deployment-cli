import os
import shutil
import traceback
from distutils import dir_util
from utils.administration import APP_ROOT_PATH
from utils.db import query_to_database
from utils.product_def import FeatureDiagram
from utils.shell import (get_script_extension, read_pid_file,
                         write_pid_to_file, run_process, stop_process)
from utils.template import fill_template


class BackendInstance():
    def __init__(self, payload, config, instance_name="backend"):
        self.config = config
        self.instance_name = instance_name
        self.payload = dict(payload)
        self.payload["instance_name"] = instance_name
        self.payload.update(
            {"db_" + k: x
             for k, x in self.config["database"].items()})

        self.SOURCE_PATH = os.path.join(
            os.path.abspath(
                os.path.expanduser(self.config["source_paths::backend"])),
            instance_name)
        self.DEPLOY_ROOT = os.path.abspath(
            os.path.expanduser(self.config["work_paths::deploy_location"]))
        self.PRODUCT_ROOT = os.path.join(self.DEPLOY_ROOT,
                                         self.payload["product_name"])
        self.DEPLOY_LOG_PATH = os.path.join(
            self.PRODUCT_ROOT,
            instance_name + self.config["logs::backend_deploy"])
        self.RUN_LOG_PATH = os.path.join(
            self.PRODUCT_ROOT,
            instance_name + self.config["logs::backend_run"])
        self.destination = os.path.join(self.PRODUCT_ROOT, instance_name, "")
        self.template_root = os.path.join(self.destination, "template")
        self.template_paths = [
            ["auth.properties"],
            ["build.xml"],
            ["build.bat"],
            ["build.sh"],
            ["config.properties"],
            ["services.properties"],
            ["run.bat"],
            ["run.sh"],
            ["src", "java", "com", "rse", "absserver", "ABSServer.java"],
        ]

    def set_destination(self, destination):
        self.destination = destination

    def is_implementation_exist(self):
        return os.path.exists(self.SOURCE_PATH)

    def build(self):
        try:
            if not self.is_implementation_exist():
                print(
                    "WARNING: Implementation of %s doesn't exist or not yet implemented."
                    " We will ignore it." % (self.instance_name, ))
                return True
            if not os.path.exists(os.path.dirname(self.DEPLOY_LOG_PATH)):
                os.makedirs(os.path.dirname(self.DEPLOY_LOG_PATH),
                            exist_ok=True)
            shutil.copytree(self.SOURCE_PATH,
                            self.destination,
                            ignore=shutil.ignore_patterns(".git"))

            self.__apply_templates()
            self.__generate_products_abs()
            self.__fix_script_permissions()
            self.__build_abs_to_java()
        except Exception:
            traceback.print_exc()
            return False
        return True

    def is_running(self):
        return read_pid_file(self.destination)

    def run(self, socket):
        socket.release()
        if os.path.exists(self.destination):
            existing_pid = read_pid_file(self.destination)
            if not existing_pid:
                with open(self.RUN_LOG_PATH, "a") as f:
                    cmd = [
                        os.path.join(self.destination,
                                     "run" + get_script_extension())
                    ]
                    proc = run_process(cmd, log=f, cwd=self.destination)
                    write_pid_to_file(proc, self.destination)

    def stop(self):
        pid = read_pid_file(self.destination)
        if pid:
            stop_process(pid)
            write_pid_to_file(None, self.destination)

    def destroy(self):
        shutil.rmtree(self.destination, ignore_errors=True)
        self.__drop_database()

    def __apply_templates(self, **kwargs):
        variables = dict(self.payload)
        variables.update(kwargs)
        for file_path in self.template_paths:
            fill_template(os.path.join(self.template_root, *file_path),
                          variables)
        dir_util.copy_tree(self.template_root, self.destination)
        dir_util.remove_tree(self.template_root)

    def __generate_products_abs(self):
        subfeature_tree = self.payload["basic_features"]
        feature_model_path = os.path.join(self.destination, "src", "abs",
                                          "framework", "FeatureModel.abs")
        feature_model = FeatureDiagram(feature_model_path)
        feature_list = feature_model.expand_default(
            subfeature_tree, root_feature_name=self.instance_name)
        feature_list.extend(
            self.payload["advanced_features"][self.instance_name])
        feature_list = list(set(feature_list))
        print("Chosen features for", self.instance_name + ":", feature_list)
        with open(
                os.path.join(self.destination, "src", "abs", "framework",
                             "Products.abs"), "w+") as f:
            f.write("product %s (%s);" %
                    (self.payload["product_name"], ", ".join(feature_list)))

    def __fix_script_permissions(self):
        os.chmod(
            os.path.join(self.destination, "build" + get_script_extension()),
            0o775)
        os.chmod(
            os.path.join(self.destination, "run" + get_script_extension()),
            0o775)

    def __build_abs_to_java(self):
        with open(self.DEPLOY_LOG_PATH, "a") as f:
            cmd = [
                os.path.join(self.destination,
                             "build" + get_script_extension())
            ]
            abs_returncode = run_process(cmd, wait=True, log=f)
        if abs_returncode != 0:
            raise IOError("%s ABS build failed with error code: %s" % (
                self.instance_name,
                abs_returncode,
            ))

    def __drop_database(self):
        user_query = fill_template(os.path.join(APP_ROOT_PATH, "templates",
                                                "drop_database.template.sql"),
                                   self.payload,
                                   write_to_file=False)
        query_to_database(user_query, self.config)
